import logo from "./logo.svg";
import "./App.css";
import { useState, useEffect } from "react";

function App() {
  const [minutes, setMinutes] = useState(25);
  const [seconds, setSeconds] = useState(0);
  const [isRunning, setIsRunning] = useState(false);
  const [timer, setTimer] = useState(25);

  useEffect(() => {
    if (minutes === 0 && seconds === 0) {
      stopTimer();
    }
  }, [minutes, seconds]);

  const onPlay = () => {
    if (!isRunning) {
      startTimer();
    } else {
      setIsRunning(false);
    }
  };

  const startTimer = () => {
    setIsRunning(true);
    if (seconds === 0 && minutes === 0) {
      setMinutes(25);
      setSeconds(0);
    }
  };

  useEffect(() => {
    let interval = null;
    if (isRunning) {
      interval = setInterval(() => {
        if (seconds === 0) {
          if (minutes === 0) {
            setIsRunning(false);
            clearInterval(interval);
          } else {
            setMinutes(minutes - 1);
            setSeconds(59);
          }
        } else {
          setSeconds(seconds - 1);
        }
      }, 1000);
    }
    return () => clearInterval(interval);
  }, [isRunning, minutes, seconds]);

  const stopTimer = () => {
    setIsRunning(false);
  };

  const onReset = () => {
    setMinutes(25);
    setSeconds(0);
    setIsRunning(false);
    setTimer(25);
  };
  const Decrement = () => {
    if (!isRunning) {
      if (timer > 25 && seconds === 0) {
        setTimer(timer - 1);
      }
    } else {
      setTimer(timer);
    }
  };
  const Increment = () => {
    if (!isRunning) {
      if (seconds === 0) {
        setTimer(timer + 1);
      }
    } else {
      setTimer(timer);
    }
  };
  useEffect(() => {
    setMinutes(timer);
  }, [timer]);
  return (
    <div className="bodyContainer">
      <div className="bgcontainer">
        <h1 className="digitaltimerHeader">Digital Timer</h1>
        <div className="divisionContainer">
          <div className="timerOuterContainer">
            <div className="runningTimer">
              <h1 className="timerText">
                {minutes < 10 ? "0" + minutes : minutes}:
                {seconds < 10 ? "0" + seconds : seconds}
              </h1>
              <p className="runningText">{isRunning ? "Running" : "Paused"}</p>
            </div>
          </div>
          <div className="inputContainer">
            <div className="buttonContainer">
              <img
                className="PlayButton"
                onClick={onPlay}
                src={
                  isRunning
                    ? "https://assets.ccbp.in/frontend/react-js/pause-icon-img.png"
                    : "https://assets.ccbp.in/frontend/react-js/play-icon-img.png"
                }
              />
              <span className="startText">{isRunning ? "Pause" : "Start"}</span>
              <img
                className="ResetButton"
                onClick={onReset}
                src="https://assets.ccbp.in/frontend/react-js/reset-icon-img.png"
              />
              <span className="ResetText">Reset</span>
            </div>
            <p className="timerSetText">Set Timer limit</p>
            <div className="timerSetContainer">
              <p className="PlusMinus" onClick={Decrement}>
                -
              </p>
              <p className="numberChanger">{timer}</p>
              <p className="PlusMinus" onClick={Increment}>
                +
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
